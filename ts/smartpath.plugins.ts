import * as os from 'os';
import * as path from 'path';
import * as url from 'url';

export { os, path, url };
