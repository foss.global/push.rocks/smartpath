/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartpath',
  version: '5.0.11',
  description: 'offers smart ways to handle paths'
}
